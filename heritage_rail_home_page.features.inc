<?php
/**
 * @file
 * heritage_rail_home_page.features.inc
 */

/**
 * Implements hook_views_api().
 */
function heritage_rail_home_page_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}
